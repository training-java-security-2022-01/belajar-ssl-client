# Mengakses HTTPS dengan Self-Signed Certificate dari Client #

1. Import sertifikat dan root CA ke dalam file PKCS12

    ```
    keytool -import -alias demo -file ../belajar-openssl/self-signed-certificate/demo.endy.muhardin.com/demo.endy.muhardin.com.crt -storetype pkcs12 -keystore src/main/resources/keystore-aplikasi.p12
    ```

    Cek isi keystore

    ```
    keytool -list -v -storetype PKCS12 -keystore src/main/resources/keystore-aplikasi.p12
    ```

2. Load file PKCS12 dari java

    ```java
    KeyStore ks = KeyStore.getInstance("PKCS12");
        ks.load(DemoHttpsClientTrustSelfSigned.class
            .getResourceAsStream("/keystore-aplikasi.p12"), "abcd1234".toCharArray());
    ```

3. Gunakan di OkHttp

4. Import certificate langsung dari website

    ```
    keytool -printcert -sslserver catfact.ninja -rfc | keytool -import -noprompt -alias catfact -keystore src/main/resources/keystore-aplikasi.p12 -storepass abcd1234
    ```
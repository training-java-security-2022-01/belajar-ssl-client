package com.muhardin.endy.belajar.ssl.client;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DemoHttpsClientTrustSelfSigned {
    public static void main(String[] args) throws Exception {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        trustCertificates(builder);

        OkHttpClient client = builder.build();

        Request request = new Request.Builder()
        //.url("https://localhost:8080/api/halo")
        .url("https://catfact.ninja/fact")
        .build();

        Call call = client.newCall(request);
        Response response = call.execute();
        String responseBody = response.body().string();
        System.out.println(responseBody);
    }

    private static void trustCertificates(OkHttpClient.Builder builder)
            throws Exception {
        SSLContext sslContext = SSLContext.getInstance("SSL");
        TrustManager[] tms = loadPkcs12();
        sslContext.init(null, tms, new java.security.SecureRandom());
        builder.sslSocketFactory(sslContext.getSocketFactory(), (X509TrustManager) tms[0]);
        builder.hostnameVerifier(new HostnameVerifier() {

            @Override
            public boolean verify(String hostname, SSLSession session) {
                System.out.println("Hostname : "+hostname);
                return true;
            }
            
        });
    }

    private static TrustManager[] loadPkcs12() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException{
        KeyStore ks = KeyStore.getInstance("PKCS12");
        ks.load(DemoHttpsClientTrustSelfSigned.class
            .getResourceAsStream("/keystore-aplikasi.p12"), "abcd1234".toCharArray());

        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(ks);
        TrustManager[] tms = tmf.getTrustManagers();
        System.out.println("Jumlah trust manager : "+tms.length);
        return tms;
    }
}

package com.muhardin.endy.belajar.ssl.client;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DemoHttpsClientTrustAll {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        trustAllCertificates(builder);
        //builder.certificatePinner(certificatePinner());

        OkHttpClient client = builder.build();

        Request request = new Request.Builder()
        .url("https://localhost:8080/api/halo")
        .build();

        Call call = client.newCall(request);
        Response response = call.execute();
        String responseBody = response.body().string();
        System.out.println(responseBody);
    }

    private static void trustAllCertificates(OkHttpClient.Builder builder)
            throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, new TrustManager[] { buildTrustAllManager() }, new java.security.SecureRandom());
        builder.sslSocketFactory(sslContext.getSocketFactory(), (X509TrustManager) buildTrustAllManager());
        builder.hostnameVerifier(new HostnameVerifier() {

            @Override
            public boolean verify(String hostname, SSLSession session) {
                System.out.println("Hostname : "+hostname);
                return true;
            }
            
        });
    }

    private static TrustManager buildTrustAllManager(){
        return new X509TrustManager() {
            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
            }
        
            @Override 
            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
            }
        
            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[] {};
            }
        };
    }
}
